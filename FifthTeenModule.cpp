
#include <iostream>
void FindOddNumbers(int Limit, bool isOdd) {

    for (int i = 0; i <= Limit; i++) {
        if (isOdd) {
            if (i % 2 == 0) {
                std::cout << i << "\n";
            }
        }
        else if (i % 2 != 0) {
            std::cout << i << "\n";
        }
    }

}

int main() {
    int Limit;
    bool isOdd;

    std::cin >> Limit;

    const int variable = 10;

    for (int i = 0; i <= variable; i++) {
        if (i % 2 == 0) {
            std::cout << i;
        }
    }

    FindOddNumbers(Limit, true);

}
